<?php
$uid = posix_getuid();
if($uid !=0) {
	echo "need root run this\n";
	die();
}
$iosspecdir = '/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/Library/Xcode/Specifications/';
if(!file_exists($iosspecdir)) {
	$cmd = 'mkdir -p "' . $iosspecdir . '"';
	echo $cmd . "\r\n";
	passthru($cmd);
}

$prjtemplate = '/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/Library/Xcode/Templates/Project Templates/iOS/Framework & Library';
$staticdir = 'Cocoa Touch Static Library.xctemplate';
$dynamicdir = 'Cocoa Touch Dynamic Library.xctemplate';

if(!file_exists($prjtemplate . "/" . $dynamicdir) || !file_exists($prjtemplate . "/TemplateIcon.icns")) {
	$cmd = 'mkdir  "' . $prjtemplate . "/" . $dynamicdir . '"';
	echo $cmd . "\r\n";
	passthru($cmd);

	$cmd = "cp -r -v \"" . $prjtemplate . "/" .$staticdir . "/\" \"" . $prjtemplate . "/" . $dynamicdir . "/\"";
	echo $cmd . "\r\n";
	passthru($cmd);
	$tempfile = $prjtemplate . "/" . $dynamicdir . "/TemplateInfo.plist";
	$content = file_get_contents($tempfile) ;
	$content = str_replace("static","dynamic",$content);
	$content = str_replace("Static","Dynamic",$content);
	file_put_contents($tempfile,$content);
	echo "done\n";
}